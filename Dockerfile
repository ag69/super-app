FROM python:3


WORKDIR /usr/src/app

COPY . /usr/src/app
RUN pip install --no-cache-dir -r requirements.txt


#Recomendation to open ports, use upercase. Ex.: docker run -P
#EXPOSE 8002

# Run whois server
 ENTRYPOINT ["python", "main.py"]
